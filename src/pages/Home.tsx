import React from 'react';
import {View, Text, StyleSheet, Button, ScrollView} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {Header} from 'react-native-elements';
import {AppCard} from '../../src/components/AppCard';
export const Home = (props: any) => {
  return (
    <View style={styles.root}>
      <Text>Home</Text>
      <ScrollView>
        <AppCard />
        <AppCard />
        <AppCard />
        <AppCard />
      </ScrollView>
      <Button
        title="go to Category Tabs"
        onPress={() => {
          Navigation.push(props.componentId, {
            component: {
              name: 'Categories',
              options: {
                bottomTabs: {visible: false, drawBehind: true, animate: true},
              },
              passProps: {
                name: 'John Doe',
                status: 'online',
              },
            },
          });
        }}
      />
    </View>
  );
};

Home.options = {
  bottomTab: {
    text: 'Home',
  },
};

Navigation.events().registerAppLaunchedListener(async () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'Home',
            },
          },
        ],
      },
    },
  });
});

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'whitesmoke',
  },
});
