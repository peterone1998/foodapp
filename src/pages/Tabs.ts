export const mainRoot = {
  root: {
    bottomTabs: {
      children: [
        {
          stack: {
            children: [
              {
                component: {
                  name: 'Home',
                },
              },
            ],
            options: {
              bottomTab: {
                icon: require('../assets/image/home.png')
              }
            }
          },
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: 'Settings',
                },
              },
            ],
            options: {
              bottomTab: {
                icon: require('../assets/image/settings.png')
              }
            }
          },
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: 'User',
                },
              },
            ],
            options: {
              bottomTab: {
                icon: require('../assets/image/user.png')
              }
            }
          },
        },
      ],
    },
  },
};
