import React from 'react';
import {View, Text, Button, StyleSheet, TouchableOpacity} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {Home} from './pages/Home';
import {Settings} from './pages/Settings';
import {Categories} from './pages/Categories';
import {Login} from './pages/Login';
import {Image} from 'react-native-elements';
import {User} from './pages/User';

export default function App(props: any) {
  return (
    <View style={styles.container}>
      <Image
        source={require('./assets/image/icon.png')}
        style={{width: 200, height: 200, marginBottom: 20}}
      />
      <TouchableOpacity
        style={styles.loginBtn}
        onPress={() => {
          Navigation.push(props.componentId, {
            component: {
              name: 'Login',
            },
          });
        }}>
        <Text style={styles.loginText}>Let started</Text>
      </TouchableOpacity>
    </View>
  );
}

Navigation.registerComponent('Login', () => Login);
Navigation.registerComponent('Home', () => Home);
Navigation.registerComponent('Settings', () => Settings);
Navigation.registerComponent('Categories', () => Categories);
Navigation.registerComponent('User', () => User);

Navigation.setDefaultOptions({
  statusBar: {
    backgroundColor: '#4d089a',
  },
  topBar: {
    visible: false,
    height: 0,
  },
  bottomTab: {
    fontSize: 14,
    selectedFontSize: 14,
    textColor: 'white',
    selectedTextColor: 'red',
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fb5b5a',
    marginBottom: 40,
  },
  loginBtn: {
    width: '50%',
    backgroundColor: '#fb5b5a',
    borderRadius: 25,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  loginText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
});
